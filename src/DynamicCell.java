import javax.swing.*;
import java.awt.*;

/**
 * Button cell that represents a dynamic cell in the puzzle.
 */
class DynamicCell extends JButton implements GridCell {
    private int value = 0;

    /**
     * Initializes the object.
     * @param borders which edges of the cell have borders
     * @param puzzleGame the overall puzzle
     * @param index the index of the cell in the puzzle
     */
    DynamicCell(int[] borders, PuzzleGame puzzleGame, int index) {
        this.setText("_");
        this.setHorizontalAlignment(JLabel.CENTER);
        this.setBorder(BorderFactory.createCompoundBorder(
                        BorderFactory.createMatteBorder(borders[0], borders[1], borders[2], borders[3], Color.black),
                        BorderFactory.createMatteBorder(1 - borders[0], 1 - borders[1], 1 - borders[2], 1 - borders[3], Color.gray)
                ));
        this.addActionListener(e -> new ValueSetter(this, puzzleGame, index));
        this.setBackground(Color.white);
    }

    @Override
    public void turnRed() {
        this.setBackground(Color.red);
    }

    @Override
    public void turnWhite() {
        this.setBackground(Color.white);
    }

    @Override
    public void turnGreen() {
        this.setBackground(Color.green);
    }

    @Override
    public int getValue() {
        return this.value;
    }

    /**
     * Sets the value of value.
     * @param value the new value of value
     */
    void setValue(int value) {
        this.value = value;
        this.setText(Integer.toString(value));
    }
}
