/**
 * The puzzle that is played
 */
class PuzzleGame implements Puzzle {
    private GridCell[] cells;

    /**
     * Initialization of the object
     */
    PuzzleGame() {
        this.cells = new GridCell[81];
    }

    /**
     * Sets the value of a cell
     * @param index the index of the cell being set
     * @param cell the template to set the cell as
     */
    void setCell(int index, GridCell cell) {
        this.cells[index] = cell;
    }

    @Override
    public boolean testPartialRowValidity(int indexInRow) {
        int firstRowValue = indexInRow - indexInRow % 9;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int index = firstRowValue; index < firstRowValue + 9; index++) {
            if (this.cells[index].getValue() != 0) {
                if (!valueFound[this.cells[index].getValue() - 1]) {
                    valueFound[this.cells[index].getValue() - 1] = true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean testPartialColumnValidity(int indexInColumn) {
        int firstColumnValue = indexInColumn % 9;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int index = firstColumnValue; index < 81; index += 9) {
            if (this.cells[index].getValue() != 0) {
                if (!valueFound[this.cells[index].getValue() - 1]) {
                    valueFound[this.cells[index].getValue() - 1] = true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean testPartialSectionValidity(int indexInSection) {
        int firstSectionValue = indexInSection - indexInSection % 27 + indexInSection % 9 - indexInSection % 3;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int rowOffset = 0; rowOffset <= 18; rowOffset += 9) {
            for (int offset = rowOffset; offset < rowOffset + 3; offset++) {
                if (this.cells[firstSectionValue + offset].getValue() != 0) {
                    if (!valueFound[this.cells[firstSectionValue + offset].getValue() - 1]) {
                        valueFound[this.cells[firstSectionValue + offset].getValue() - 1] = true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public boolean testRowValidity(int indexInRow) {
        int firstRowValue = indexInRow - indexInRow % 9;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int index = firstRowValue; index < firstRowValue + 9; index++) {
            if (this.cells[index].getValue() != 0) {
                if (!valueFound[this.cells[index].getValue() - 1]) {
                    valueFound[this.cells[index].getValue() - 1] = true;
                } else {
                    return false;
                }
            }
        }
        for (boolean valueState : valueFound) {
            if (!valueState) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean testColumnValidity(int indexInColumn) {
        int firstColumnValue = indexInColumn % 9;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int index = firstColumnValue; index < 81; index += 9) {
            if (this.cells[index].getValue() != 0) {
                if (!valueFound[this.cells[index].getValue() - 1]) {
                    valueFound[this.cells[index].getValue() - 1] = true;
                } else {
                    return false;
                }
            }
        }
        for (boolean valueState : valueFound) {
            if (!valueState) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean testSectionValidity(int indexInSection) {
        int firstSectionValue = indexInSection - indexInSection % 27 + indexInSection % 9 - indexInSection % 3;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int rowOffset = 0; rowOffset <= 18; rowOffset += 9) {
            for (int offset = rowOffset; offset < rowOffset + 3; offset++) {
                if (this.cells[firstSectionValue + offset].getValue() != 0) {
                    if (!valueFound[this.cells[firstSectionValue + offset].getValue() - 1]) {
                        valueFound[this.cells[firstSectionValue + offset].getValue() - 1] = true;
                    } else {
                        return false;
                    }
                }
            }
        }
        for (boolean valueState : valueFound) {
            if (!valueState) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean testPuzzle() {
        for (int row = 0; row < 81; row += 9) {
            if (!this.testRowValidity(row)) {
                return false;
            }
        }
        for (int column = 0; column < 9; column++) {
            if (!this.testColumnValidity(column)) {
                return false;
            }
        }
        return !(!this.testSectionValidity(0) || !this.testSectionValidity(3) || !this.testSectionValidity(6) ||
                !this.testSectionValidity(27) || !this.testSectionValidity(30) || !this.testSectionValidity(33) ||
                !this.testSectionValidity(54) || !this.testSectionValidity(57) || !this.testSectionValidity(60));
    }

    /**
     * Turns every cell in a row red.
     * @param indexInRow an index in the row
     */
    void turnRowRed(int indexInRow) {
        int firstRowValue = indexInRow - indexInRow % 9;
        for (int index = firstRowValue; index < firstRowValue + 9; index++) {
            this.cells[index].turnRed();
        }
    }

    /**
     * Turns every cell in a column red.
     * @param indexInColumn an index in the column
     */
    void turnColumnRed(int indexInColumn) {
        int firstColumnValue = indexInColumn % 9;
        for (int index = firstColumnValue; index < 81; index += 9) {
            this.cells[index].turnRed();
        }
    }

    /**
     * Turns every cell in a section red.
     * @param indexInSection an index in the section
     */
    void turnSectionRed(int indexInSection) {
        int firstSectionValue = indexInSection - indexInSection % 27 + indexInSection % 9 - indexInSection % 3;
        for (int rowOffset = 0; rowOffset <= 18; rowOffset += 9) {
            for (int offset = rowOffset; offset < rowOffset + 3; offset++) {
                this.cells[firstSectionValue + offset].turnRed();
            }
        }
    }

    /**
     * Turns every cell in a row white.
     * @param indexInRow an index in the row
     */
    void turnRowWhite(int indexInRow) {
        int firstRowValue = indexInRow - indexInRow % 9;
        for (int index = firstRowValue; index < firstRowValue + 9; index++) {
            this.cells[index].turnWhite();
        }
    }

    /**
     * Turns every cell in a column white.
     * @param indexInColumn an index in the column
     */
    void turnColumnWhite(int indexInColumn) {
        int firstColumnValue = indexInColumn % 9;
        for (int index = firstColumnValue; index < 81; index += 9) {
            this.cells[index].turnWhite();
        }
    }

    /**
     * Turns every cell in a section white.
     * @param indexInSection an index in the section
     */
    void turnSectionWhite(int indexInSection) {
        int firstSectionValue = indexInSection - indexInSection % 27 + indexInSection % 9 - indexInSection % 3;
        for (int rowOffset = 0; rowOffset <= 18; rowOffset += 9) {
            for (int offset = rowOffset; offset < rowOffset + 3; offset++) {
                this.cells[firstSectionValue + offset].turnWhite();
            }
        }
    }

    /**
     * Turns every cell in a row green.
     * @param indexInRow an index in the row
     */
    void turnRowGreen(int indexInRow) {
        int firstRowValue = indexInRow - indexInRow % 9;
        for (int index = firstRowValue; index < firstRowValue + 9; index++) {
            if (this.testRowValidity(index) && this.testColumnValidity(index) && this.testSectionValidity(index)) {
                this.cells[index].turnGreen();
            }
        }
    }

    /**
     * Turns every cell in a column green.
     * @param indexInColumn an index in the column
     */
    void turnColumnGreen(int indexInColumn) {
        int firstColumnValue = indexInColumn % 9;
        for (int index = firstColumnValue; index < 81; index += 9) {
            if (this.testRowValidity(index) && this.testColumnValidity(index) && this.testSectionValidity(index)) {
                this.cells[index].turnGreen();
            }
        }
    }

    /**
     * Turns every cell in a section green.
     * @param indexInSection an index in the section
     */
    void turnSectionGreen(int indexInSection) {
        int firstSectionValue = indexInSection - indexInSection % 27 + indexInSection % 9 - indexInSection % 3;
        for (int rowOffset = 0; rowOffset <= 18; rowOffset += 9) {
            for (int offset = rowOffset; offset < rowOffset + 3; offset++) {
                if (this.testRowValidity(firstSectionValue + offset) && this.testColumnValidity(firstSectionValue + offset) && this.testSectionValidity(firstSectionValue + offset)) {
                    this.cells[firstSectionValue + offset].turnGreen();
                }
            }
        }
    }
}
