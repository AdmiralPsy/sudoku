/**
 * Interface for cells used in the PuzzleGame class.
 */
interface GridCell {
    /**
     * Changes the cell's background to red.
     */
    void turnRed();

    /**
     * Changes the cell's background to white.
     */
    void turnWhite();

    /**
     * Changes the cell's background to green.
     */
    void turnGreen();

    /**
     * Gets the value saved in the cell.
     *
     * @return the cell's value
     */
    int getValue();
}