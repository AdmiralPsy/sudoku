/**
 * Interface for the puzzle creation and playing classes.
 */
interface Puzzle {
    /**
     * Tests to see if the row has any duplicate values.
     * @param valueInRow an index in the target row
     * @return if there are any duplicates
     */
    boolean testPartialRowValidity(int valueInRow);

    /**
     * Tests to see if the column has any duplicate values.
     * @param valueInColumn an index in the target column
     * @return if there are any duplicates
     */
    boolean testPartialColumnValidity(int valueInColumn);

    /**
     * Tests to see if the section has any duplicate values.
     * @param valueInSection an index in the section
     * @return if there are any duplicates
     */
    boolean testPartialSectionValidity(int valueInSection);

    /**
     * Tests to see if the row has every value needed to be complete.
     * @param valueInRow an index in the target row
     * @return if it has every value
     */
    boolean testRowValidity(int valueInRow);

    /**
     * Tests to see if the column has every value needed to be complete
     * @param valueInColumn an index in the target column
     * @return if it has every value
     */
    boolean testColumnValidity(int valueInColumn);

    /**
     * Tests to see if the section has every value needed to be complete
     * @param valueInSection an index in the target section
     * @return if it has every value
     */
    boolean testSectionValidity(int valueInSection);

    /**
     * Tests to see if every index in the puzzle has a valid row, column, and section.
     * @return If every index is valid
     */
    boolean testPuzzle();
}
