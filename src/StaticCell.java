import javax.swing.*;
import java.awt.*;

/**
 * Label cell that represents a dynamic cell in the puzzle
 */
class StaticCell extends JLabel implements GridCell {
    private int value;

    /**
     * Initializes the object.
     * @param value the value of the cell
     * @param borders which edges of the cell have borders
     */
    StaticCell(int value, int[] borders) {
        this.value = value;
        this.setText(Integer.toString(value));
        this.setHorizontalAlignment(JLabel.CENTER);
        this.setBorder(BorderFactory.createMatteBorder(borders[0], borders[1], borders[2], borders[3], Color.black));
        this.setOpaque(true);
        this.setBackground(Color.white);
    }

    @Override
    public void turnRed() {
        this.setBackground(Color.red);
    }

    @Override
    public void turnWhite() {
        this.setBackground(Color.white);
    }

    @Override
    public void turnGreen() {
        this.setBackground(Color.green);
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
