import java.util.concurrent.ThreadLocalRandom;

class Square {
    private int currentValue;
    boolean[] validValues;
    boolean staticValue;

    /**
     * Initializes the object.
     */
    Square() {
        this.currentValue = 0;
        this.validValues = new boolean[]{true, true, true, true, true, true, true, true, true};
        this.staticValue = false;
    }

    void setCurrentValue(int value) {
        if (!this.staticValue) {
            this.currentValue = value;
        }
    }

    /**
     *  Returns the current value of the object.
     * @return the square's current value
     */
    int getCurrentValue() {
        return this.currentValue;
    }

    /**
     * Sets the cell to have a random value, performing recursion if the value isn't valid.
     */
    private void setRandomCurrentValue() {
        int index = ThreadLocalRandom.current().nextInt(0, 9);
        if (this.validValues[index]) {
            this.currentValue = index + 1;
        } else {
            this.setRandomCurrentValue();
        }
    }

    /**
     * Checks if any valid values still exists. Sets a random new value if there are, and returns if there are or not.
     * @return whether there are any valid values
     */
    boolean findAvailableValue() {
        boolean valueAvailable = false;
        for (boolean valueValidity : this.validValues) {
            if (valueValidity) {
                valueAvailable = true;
            }
        }
        if (valueAvailable) {
            this.setRandomCurrentValue();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Resets the current value and sets that value to be unavailable.
     */
    void invalidateCurrentValue() {
        this.validValues[this.currentValue - 1] = false;
        this.currentValue = 0;
    }

    /**
     * Resets the current value and sets all values to be available.
     */
    void resetSquare() {
        this.currentValue = 0;
        this.validValues = new boolean[]{true, true, true, true, true, true, true, true, true};
        this.staticValue = false;
    }

    boolean getStaticity() {
        return this.staticValue;
    }
}
