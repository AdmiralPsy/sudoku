import javax.swing.*;
import java.awt.*;

/**
 * Contract: All #'s are correct -> victory!
 *
 * Purpose: Make it so the user knows when they have all the right numbers entered
 *
 * Example: All the correct numbers are entered and a box pulls up saying "Victory!"
 */
public class Victory extends JFrame {
    public Victory() {
        this.initGUI();
        JPanel panel = new JPanel();
        JLabel label = new JLabel("You've won!");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));
        panel.add(label);
        JButton button = new JButton("OK");
        button.addActionListener(e -> System.exit(0));
        panel.add(button);
        this.add(panel);
        this.repaint();
        this.revalidate();
    }

    private void initGUI() {
        this.setTitle("Victory!");
        this.setSize(100, 100);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
