import javax.swing.*;
import java.awt.*;

/**
 * Main game object.
 */
public class Sudoku extends JFrame {
    /**
     * The script to run for the project.
     * @param args command line input
     */
    public static void main(String[] args) {
        new Sudoku();
    }

    /**
     * Initializes the object.
     */
    private Sudoku() {
        // Initial sudoku object setup.
        this.initGUI();
        JPanel grid = new JPanel();
        grid.setLayout(new GridLayout(9, 9));
        // Generates the puzzle.
        PuzzleGenerator puzzleGenerator = new PuzzleGenerator((int) (Math.random() * 30) + 20);
        // Initializes the puzzle game object.
        PuzzleGame puzzleGame = new PuzzleGame();
        // Moves all of the cells to the puzzle game and creates the main sudoku layout.
        this.createGrid(grid, puzzleGenerator, puzzleGame);
        this.add(grid);
        // Refreshes the main sudoku screen.
        this.repaint();
        this.revalidate();
    }

    /**
     * Creates the general settings for the JFrame.
     */
    private void initGUI() {
        this.setTitle("Sudoku");
        // Sets the size of the sudoku window and centers it.
        this.setSize(800, 600);
        this.setLocationRelativeTo(null);
        // Sets the script to end when the sudoku window closes.
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        // Causes the sudoku window to appear.
        this.setVisible(true);
    }

    /**
     * Turns every cell from the puzzle generator into a cell in the puzzle game and creates its JComponent.
     * @param grid the panel for the main window
     * @param puzzleGenerator the generated sudoku puzzle
     * @param puzzleGame the playable sudoku puzzle
     */
    private void createGrid(JPanel grid, PuzzleGenerator puzzleGenerator, PuzzleGame puzzleGame) {
        // Sets up the objects to be used in the loop
        StaticCell staticCell;
        DynamicCell dynamicCell;
        GridCell gridCell;
        JComponent jComponent;
        // Runs through every cell in a sudoku puzzle.
        for (int i = 0; i < 81; i++) {
            // Creates a list representing if each side of a cell has a border.
            int[] borders = new int[]{
                    // Tests if the cell is on the 0th, 3rd, or 6th row. 1 == true, 0 == false.
                    (i % 27 < 9) ? 1 : 0,
                    // Tests if the cell is on the 0th, 3rd, or 6th column. 1 == true, 0 == false.
                    (i % 3 == 0) ? 1 : 0,
                    // Tests if the cell is on the 2nd, 5th, or 8th row. 1 == true, 0 == false.
                    (18 <= i % 27 && i % 27 < 27) ? 1 : 0,
                    // Tests if the cell is on the 2nd, 5th, or 8th column. 1 == true, 0 == false.
                    (i % 3 == 2) ? 1 : 0
            };
            // Creates a staticCell for any static values, and a dynamic cell for dynamic values.
            if (puzzleGenerator.getSquareStaticity(i)) {
                staticCell = new StaticCell(puzzleGenerator.getSquareValue(i), borders);
                gridCell = staticCell;
                jComponent = staticCell;
            } else {
                dynamicCell = new DynamicCell(borders, puzzleGame, i);
                gridCell = dynamicCell;
                jComponent = dynamicCell;
            }
            // Adds the new cell to the sudoku window.
            grid.add(jComponent);
            // Adds the new cell to the puzzle game.
            puzzleGame.setCell(i, gridCell);
        }

        // Sets all of the colors for the puzzle.
        for (int index = 0; index < 81; index++) {
            // Sets the cell to be green if the row is correct and white if not.
            if (puzzleGame.testRowValidity(index)) {
                puzzleGame.turnRowGreen(index);
            } else {
                puzzleGame.turnRowWhite(index);
            }
            // Sets the cell to be green if the column is correct and white if not.
            if (puzzleGame.testColumnValidity(index)) {
                puzzleGame.turnColumnGreen(index);
            } else {
                puzzleGame.turnColumnWhite(index);
            }
            // Sets the cell to be green if the section is correct and white if not.
            if (puzzleGame.testSectionValidity(index)) {
                puzzleGame.turnSectionGreen(index);
            } else {
                puzzleGame.turnSectionWhite(index);
            }
        }
    }
}
