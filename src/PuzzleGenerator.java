import java.util.concurrent.ThreadLocalRandom;

/**
 * The object that generates a puzzle.
 */
class PuzzleGenerator implements Puzzle {
    private Square[] squares;

    /**
     * Initializes the object.
     * @param cellsToRemove how many cells to remove from the final puzzle
     */
    PuzzleGenerator(int cellsToRemove) {
        this.squares = new Square[81];
        this.generateCompletePuzzle();
        for (int index = 0; index < 81; index++) {
            this.squares[index].staticValue = true;
        }
        this.setDynamicSquares(cellsToRemove);
    }

    /**
     * Gets the value of a square from the puzzle.
     * @param square the index of the square
     * @return the square's value
     */
    int getSquareValue(int square) {
        return this.squares[square].getCurrentValue();
    }

    /**
     * Gets if the square is static.
     * @param square the index of the square
     * @return the square's staticity
     */
    boolean getSquareStaticity(int square) {
        return this.squares[square].staticValue;
    }

    /**
     * Generates a complete and valid puzzle.
     */
    private void generateCompletePuzzle() {
        // Sets every index in the squares list to be a new square.
        for (int index = 0; index < 81; index++) {
            this.squares[index] = new Square();
        }

        // Runs until every cell is set.
        for (int squareIndex = 0; squareIndex < 81; squareIndex++) {
            // Attempts to set a value for the current square.
            if (!this.squares[squareIndex].findAvailableValue()) {
                // Runs if the current square has no available values.
                // Resets the current square.
                this.squares[squareIndex].resetSquare();
                // Moves back a square.
                squareIndex--;
                // Sets the current value of the square to be invalid.
                this.squares[squareIndex].invalidateCurrentValue();
                // Moves back a square.
                squareIndex--;
                // Ends with the program trying to set the value of the square before the square originally being worked.
            } else if (!this.testPartialRowValidity(squareIndex) ||
                    !this.testPartialColumnValidity(squareIndex) ||
                    !this.testPartialSectionValidity(squareIndex)) {
                // Runs the puzzle is no longer valid.
                // Sets the current value to be invalid.
                this.squares[squareIndex].invalidateCurrentValue();
                // Moves back a square.
                squareIndex--;
                // Ends with the program trying to set the value of the square that was originally being worked on.
            }
            // If no errors were found, leaves the program trying to set the value of the square after the square originally being worked on.
        }

        // Runs a recursion if the puzzle is invalid for some reason.
        if (!this.testPuzzle()) {
            this.generateCompletePuzzle();
        }
    }

    /**
     * Sets a given number of squares to be removed from the puzzle and set to be dynamic.
     * @param cellsToRemove the number of cell to remove
     */
    private void setDynamicSquares(int cellsToRemove) {
        // Repeatedly removed cells randomly from the puzzle and sets them to be dynamic.
        int squareForRemoval;
        for (int removedIndex = 0; removedIndex < cellsToRemove; removedIndex++) {
            squareForRemoval = ThreadLocalRandom.current().nextInt(0, 81);
            if (this.squares[squareForRemoval].getStaticity()) {
                // Sets the cell to be dynamic if it isn't already.
                this.squares[squareForRemoval].staticValue = false;
                this.squares[squareForRemoval].setCurrentValue(0);
            } else {
                // Moves back an index if it is already dynamic, making a repeat attempt.
                removedIndex--;
            }
        }
    }

    /**
     * Tests if a row has any duplicate values.
     * @param valueInRow an index in the row
     * @return if the row was valid
     */
    @Override
    public boolean testPartialRowValidity(int valueInRow) {
        int firstRowValue = valueInRow - valueInRow % 9;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int index = firstRowValue; index < firstRowValue + 9; index++) {
            if (this.squares[index].getCurrentValue() != 0) {
                if (!valueFound[this.squares[index].getCurrentValue() - 1]) {
                    valueFound[this.squares[index].getCurrentValue() - 1] = true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Tests if a column has any duplicate values.
     * @param valueInColumn an index in the column
     * @return if the column was valid
     */
    @Override
    public boolean testPartialColumnValidity(int valueInColumn) {
        int firstColumnValue = valueInColumn % 9;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int index = firstColumnValue; index < 81; index += 9) {
            if (this.squares[index].getCurrentValue() != 0) {
                if (!valueFound[this.squares[index].getCurrentValue() - 1]) {
                    valueFound[this.squares[index].getCurrentValue() - 1] = true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Tests if a section has any duplicate values.
     * @param valueInSection an index in the section
     * @return if the section was valid
     */
    @Override
    public boolean testPartialSectionValidity(int valueInSection) {
        int firstSectionValue = valueInSection - valueInSection % 27 + valueInSection % 9 - valueInSection % 3;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int rowOffset = 0; rowOffset <= 18; rowOffset += 9) {
            for (int offset = rowOffset; offset < rowOffset + 3; offset++) {
                if (this.squares[firstSectionValue + offset].getCurrentValue() != 0) {
                    if (!valueFound[this.squares[firstSectionValue + offset].getCurrentValue() - 1]) {
                        valueFound[this.squares[firstSectionValue + offset].getCurrentValue() - 1] = true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Tests if a row has every value.
     * @param valueInRow an index in the row
     * @return if the row had every value
     */
    @Override
    public boolean testRowValidity(int valueInRow) {
        int firstRowValue = valueInRow - valueInRow % 9;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int index = firstRowValue; index < firstRowValue + 9; index++) {
            if (this.squares[index].getCurrentValue() != 0) {
                if (!valueFound[this.squares[index].getCurrentValue() - 1]) {
                    valueFound[this.squares[index].getCurrentValue() - 1] = true;
                } else {
                    return false;
                }
            }
        }
        for (boolean valueState : valueFound) {
            if (!valueState) {
                return false;
            }
        }
        return true;
    }

    /**
     * Tests if a column has every value.
     * @param valueInColumn an index in the column
     * @return if the column had every value
     */
    @Override
    public boolean testColumnValidity(int valueInColumn) {
        int firstColumnValue = valueInColumn % 9;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int index = firstColumnValue; index < 81; index += 9) {
            if (this.squares[index].getCurrentValue() != 0) {
                if (!valueFound[this.squares[index].getCurrentValue() - 1]) {
                    valueFound[this.squares[index].getCurrentValue() - 1] = true;
                } else {
                    return false;
                }
            }
        }
        for (boolean valueState : valueFound) {
            if (!valueState) {
                return false;
            }
        }
        return true;
    }

    /**
     * Tests if a section has every value.
     * @param valueInSection an index in the section
     * @return if the section had every value
     */
    @Override
    public boolean testSectionValidity(int valueInSection) {
        int firstSectionValue = valueInSection - valueInSection % 27 + valueInSection % 9 - valueInSection % 3;
        boolean[] valueFound = {false, false, false, false, false, false, false, false, false};
        for (int rowOffset = 0; rowOffset <= 18; rowOffset += 9) {
            for (int offset = rowOffset; offset < rowOffset + 3; offset++) {
                if (this.squares[firstSectionValue + offset].getCurrentValue() != 0) {
                    if (!valueFound[this.squares[firstSectionValue + offset].getCurrentValue() - 1]) {
                        valueFound[this.squares[firstSectionValue + offset].getCurrentValue() - 1] = true;
                    } else {
                        return false;
                    }
                }
            }
        }
        for (boolean valueState : valueFound) {
            if (!valueState) {
                return false;
            }
        }
        return true;
    }

    /**
     * Tests if the puzzle is completely correct.
     * @return if the puzzle was completely correct
     */
    @Override
    public boolean testPuzzle() {
        for (int row = 0; row < 81; row += 9) {
            if (!this.testRowValidity(row)) {
                return false;
            }
        }
        for (int column = 0; column < 9; column++) {
            if (!this.testColumnValidity(column)) {
                return false;
            }
        }
        return !(!this.testSectionValidity(0) || !this.testSectionValidity(3) || !this.testSectionValidity(6) ||
                !this.testSectionValidity(27) || !this.testSectionValidity(30) || !this.testSectionValidity(33) ||
                !this.testSectionValidity(54) || !this.testSectionValidity(57) || !this.testSectionValidity(60));
    }
}