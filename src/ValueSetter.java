import javax.swing.*;
import java.awt.*;

/**
 * An extended JFrame used to set values in the overall puzzle game
 */
class ValueSetter extends JFrame {
    /**
     * Initializes the object.
     * @param cell the cell being set
     * @param puzzleGame the overall puzzle game
     * @param index the index of the cell being set
     */
    ValueSetter(DynamicCell cell, PuzzleGame puzzleGame, int index) {
        this.initGUI();
        JPanel setters = new JPanel();
        setters.setLayout(new GridLayout(10, 1));
        this.createSetters(setters, cell, puzzleGame, index);
        this.add(setters);
        this.repaint();
        this.revalidate();
    }

    /**
     * Creates the general settings for the JFrame.
     */
    private void initGUI() {
        this.setTitle("Value Setter");
        this.setSize(200, 600);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Creates the layout for the panel used to set the values of cells.
     * @param setters the setting panel for the JFrame
     * @param cell the cell being set
     * @param puzzleGame the overall puzzle being played
     * @param index the index of the cell being set
     */
    private void createSetters(JPanel setters, DynamicCell cell, PuzzleGame puzzleGame, int index) {
        JLabel label = new JLabel("Value Setters");
        label.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));
        label.setHorizontalAlignment(SwingConstants.CENTER);
        setters.add(label);
        JButton button;
        for (int i = 1; i <= 9; i++) {
            button = new JButton();
            button.setText(Integer.toString(i));
            int finalI = i;
            button.addActionListener(e -> {
                cell.setValue(finalI);
                if (puzzleGame.testPuzzle()) {
                    puzzleGame.turnRowGreen(index);
                    puzzleGame.turnColumnGreen(index);
                    puzzleGame.turnSectionGreen(index);
                    new Victory();
                } else {
                    puzzleGame.turnRowGreen(index);
                    puzzleGame.turnColumnGreen(index);
                    puzzleGame.turnSectionGreen(index);

                    if (!puzzleGame.testRowValidity(index)) {
                        puzzleGame.turnRowWhite(index);
                    }
                    if (!puzzleGame.testColumnValidity(index)) {
                        puzzleGame.turnColumnWhite(index);
                    }
                    if (!puzzleGame.testSectionValidity(index)) {
                        puzzleGame.turnSectionWhite(index);
                    }

                    if (!puzzleGame.testPartialRowValidity(index)) {
                        puzzleGame.turnRowRed(index);
                    }
                    if (!puzzleGame.testPartialColumnValidity(index)) {
                        puzzleGame.turnColumnRed(index);
                    }
                    if (!puzzleGame.testPartialSectionValidity(index)) {
                        puzzleGame.turnSectionRed(index);
                    }
                }
                this.dispose();
            });
            setters.add(button);
        }
    }
}
